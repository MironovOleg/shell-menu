const path = require("path");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const CopyPlugin = require("copy-webpack-plugin");

const PATH = {
  build: path.resolve(__dirname, "../build"),
};

module.exports = {
  target: "node",
  mode: "production",
  entry: "./index.ts",
  output: {
    filename: "index.js",
    path: PATH.build,
    library: "library",
    libraryTarget: "umd",
  },
  resolve: {
    extensions: [".ts", ".js", ".json"],
  },
  node: {
    global: false,
    __filename: false,
    __dirname: false,
  },
  module: {
    rules: [
      {
        test: /\.ts?$/,
        loader: "awesome-typescript-loader",
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin({}),
    new CopyPlugin({
      patterns: [
        { from: "package.json", to: "./" },
        { from: "readme.md", to: "./" },
        { from: ".npmignore", to: "./" },
        { from: "resource", to: "./resource" },
      ],
    }),
  ],
};
