import * as readline from "readline";

type Question = {
  text: string;
  cb: (response: string) => void | Promise<void>;
};

type TOption = {
  label: string;
  menu?: Menu;
  cb?: () => void | Promise<void>;
  question?: Question | Question[];
};

type TParams = {
  options?: TOption[];
  subTitle?: string;
};

type TKey = {
  ctrl: boolean;
  name: string;
};

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

class Menu {
  private readonly title: string;
  private readonly subTitle?: string;
  private isActive: boolean = false;
  private options: TOption[] = [];
  private index: number = 0;
  private countOptions: number = 0;
  private static listen: boolean = false;
  public static activeMenu: Menu;
  public static useBack: boolean = true;
  private static history: Menu[] = [];
  private renderedBack: boolean = false;

  constructor(title: string, params: TParams = {}) {
    this.title = title;
    this.subTitle = params.subTitle;
    this.setOptions(params.options);
  }

  private addOption(option: TOption): Menu {
    this.options.push(option);
    this.countOptions = this.options.length - 1;
    return this;
  }

  private setOptions(options: TOption[] = []): Menu {
    this.options = options;
    this.countOptions = this.options.length - 1;
    return this;
  }

  private down(): void {
    this.index++;
    if (this.index > this.countOptions) {
      this.index = 0;
    }
    this.render();
  }

  private up(): void {
    this.index--;
    if (this.index < 0) {
      this.index = this.countOptions;
    }
    this.render();
  }

  private setActive(active: boolean): void {
    this.isActive = active;
    if (active) {
      if (Menu.useBack && Menu.history.length > 0 && !this.renderedBack) {
        this.addOption({
          label: "back",
          cb: () => {
            const menu = Menu.history.pop();
            if (menu) {
              this.index = 0;
              menu.index = 0;
              this.setActive(false);
              menu.render();
            }
          },
        });
        this.renderedBack = true;
      }
      Menu.activeMenu = this;
    }
  }

  private async selectOption(): Promise<void> {
    const opt: TOption = this.options[this.index];
    if (opt.menu) {
      this.setActive(false);
      Menu.history.push(this);
      opt.menu.render();
    } else if (typeof opt.cb === "function") {
      await opt.cb();
    } else if (opt.question) {
      Menu.exit();
      if (!Array.isArray(opt.question)) {
        opt.question = [opt.question];
      }
      for (let question of opt.question) {
        console.clear();
        await this.sendQuestion(question);
      }
    }
  }

  private async sendQuestion(question: Question): Promise<void> {
    return new Promise((resolve) => {
      rl.question(question.text, async (res: string) => {
        await question.cb(res);
        return resolve();
      });
    });
  }

  public static exit(): void {
    Menu.listen = false;
    process.stdin.setRawMode(false);
    process.stdin.removeListener("keypress", Menu.handleKeyPress);
  }

  private static async handleKeyPress(_str: string, key: TKey) {
    if (key.ctrl && key.name === "c") {
      process.exit();
    } else if (Menu.activeMenu) {
      if (key.name === "up") {
        Menu.activeMenu.up();
      }
      if (key.name === "down") {
        Menu.activeMenu.down();
      }
      if (key.name === "return") {
        await Menu.activeMenu.selectOption();
      }
    }
  }

  private static listenKeyBoard(): void {
    Menu.listen = true;
    readline.emitKeypressEvents(process.stdin);
    process.stdin.setRawMode(true);
    process.stdin.on("keypress", Menu.handleKeyPress);
  }

  public render(): void {
    if (!this.isActive) {
      this.setActive(true);
    }
    if (!Menu.listen) {
      Menu.listenKeyBoard();
    }
    console.clear();
    console.log("\x1b[33m%s\x1b[0m", this.title);
    if (this.subTitle) {
      console.log(this.subTitle);
    }
    for (let i = 0; i < this.options.length; i++) {
      const opt = this.options[i];
      if (i === this.index) {
        console.log(`> ${opt.label}`);
      } else {
        console.log(`  ${opt.label}`);
      }
    }
  }
}

export = Menu;
