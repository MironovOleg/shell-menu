## Usage

```javascript
import Menu from "shell-main-menu";
//or
const Menu = require('shell-main-menu');


const menu = new Menu("this title", { //title
	subTitle: "this sub title", //subTitle
	options: [{
		label: string; //label  item from menu
		menu?: Menu; //sub menu
		cb?: () => void; //handle or action for selected item menu
		question?: {
          text: string; // text your question
          cb: () => void; //callback for user response
        }; // object or array objects
	}], 
});


menu.render();
```
## About

use arrow UP or DOWN for chenge select item menu

|  method | descripton  |
| ------------ | ------------ |
| render  |  render menu on terminal  |
| static exit | disable menu and remove listener "keyboard" |



|  param | descripton  |
| ------------ | ------------ |
| title: string | title for menu |
| subTitle?: string | sub-title for menu |
| statuc useBack: boolean  |  render menu item for back to parent |
| statuc useBack: boolean  |  render menu item for back to parent | 

## Example

![example](https://gitlab.com/MironovOleg/shell-menu/-/raw/master/resource/example.png "example")
